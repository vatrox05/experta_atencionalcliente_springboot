package ar.com.experta.atencionalcliente;

import java.lang.reflect.Method;

import org.testng.ITestContext;
import org.testng.annotations.DataProvider;
/**
 * Created by Vatrox10 on 23/06/2017.
 */
public class ExpertaAtencionClienteTestData {

    //String usuario, String verificacion, int pwid, String hash, String poliza, String cuit
    /*{
        "usuario":"ART",
            "verificacion":"Nuevo",
            "pwid":315,
            "hash":"ULIYWRZYXIJRCRNSWBXMFKGEPQXJWUQY",
            "poliza":"8801",
            "cuit":"T30663205931"
    }*/
    @DataProvider(name="getDataAlicuotas")
    public static Object[][] getDataAlicuotas() {
        return new Object[][]{
                                {"ART","Nuevo","315", "ULIYWRZYXIJRCRNSWBXMFKGEPQXJWUQY", "8801","T30663205931"}
        };
    }

    @DataProvider(name="getDataDenunciaFormal")
    public static Object[][] getDataDenunciaFormal() {
        return new Object[][]{

                {"ART","Nuevo","315", "ULIYWRZYXIJRCRNSWBXMFKGEPQXJWUQY", "8801","T30663205931"}

        };
    }
    @DataProvider(name="getDataCobranzas")
    public static Object[][] getDataCobranzas() {
        return new Object[][]{

                {"ART","Nuevo","315", "ULIYWRZYXIJRCRNSWBXMFKGEPQXJWUQY", "8801","T30663205931"}

        };
    }
    @DataProvider(name="getDataDiasBaja")
    public static Object[][] getDataDiasBaja() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}

        };
    }
    @DataProvider(name="getDataFechaAudiencia")
    public static Object[][] getDataFechaAudiencia() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}

        };
    }
    @DataProvider(name="getDataFechaDictamen")
    public static Object[][] getDataFechaDictamen() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}

        };
    }
    @DataProvider(name="getDataFechaReingreso")
    public static Object[][] getDataFechaReingreso() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}

        };
    }
    @DataProvider(name="getDataLiquidacion")
    public static Object[][] getDataLiquidacion() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17","21/07/17"}

        };
    }
    @DataProvider(name="getDataMotivoRechazo")
    public static Object[][] getDataMotivoRechazo() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}

        };
    }
    @DataProvider(name="getDataNominaLog")
    public static Object[][] getDataNominaLog() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}

        };
    }
    @DataProvider(name="getDataNomina")
    public static Object[][] getDataNomina() {
        return new Object[][]{

                {"art","T30663205931"}

        };
    }
    @DataProvider(name="getDataNroContrato")
    public static Object[][] getDataNroContrato() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "09834209003"}

        };
    }

    @DataProvider(name="getDataNroSiniestro")
    public static Object[][] getDataNroSiniestro() {
        return new Object[][]{

                {"T30663205931","21/02/17"}

        };
    }
    @DataProvider(name="getDataPrestadorMedico")
    public static Object[][] getDataPrestadorMedico() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}

        };
    }
    @DataProvider(name="getDataProximoTurno")
    public static Object[][] getDataProximoTurno() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}

        };
    }
    @DataProvider(name="getDataSaldo")
    public static Object[][] getDataSaldo() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}

        };
    }

    @DataProvider(name="getDataSiniestro")
    public static Object[][] getDataSiniestro() {
        return new Object[][]{

                {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","23/01/16"}

        };
    }
    @DataProvider(name="getDataUsuarioExperta")
    public static Object[][] getDataUsuarioExperta() {
        return new Object[][]{

                {"30663205931","art"}

        };
    }
    @DataProvider(name="getDataVerificacionAcc")
    public static Object[][] getDataVerificacionAcc() {
        return new Object[][]{

                {"art","nuevo","23/01/16", "T30663205931", "8gfsg801","5000","uti"}

        };
    }


}
