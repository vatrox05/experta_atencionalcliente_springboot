package ar.com.experta.atencionalcliente;

import ar.com.experta.atencionalcliente.model.*;

import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.TestNG;
import org.testng.annotations.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import ar.com.experta.atencionalcliente.service.AtencionAlClienteService;
import org.testng.xml.XmlSuite;

import java.util.ArrayList;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

/*import org.junit.Test;
import org.junit.runner.RunWith;*/

@SpringBootTest
@DirtiesContext
public class ExpertaAtencionalclienteApplicationTests extends AbstractTestNGSpringContextTests {


    public static void main(String[] args) {

        TestNG testNG = new TestNG();
        testNG.setXmlPathInJar("C:\\martin\\jdev\\projects\\SpringBoot\\expertaGit\\experta-atencionalcliente\\AtencionAlClienteTests.xml");
        testNG.run();

    }

	@Autowired
    AtencionAlClienteService atencionAlClienteService;


	@Test (dataProvider="getDataAlicuotas", dataProviderClass=ExpertaAtencionClienteTestData.class)
	public void testConsultaAlicuotas(String usuario, String verificacion, String pwid, String hash, String poliza, String cuit) throws Exception {

		ConsultaAlicuotasRequest consultaAlicuotasRequest = new ConsultaAlicuotasRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuit);

		ConsultaAlicuotasResponse consultaAlicuotasResponse = atencionAlClienteService.consultaAlicuotas(consultaAlicuotasRequest);

        String derivar = consultaAlicuotasResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");
		
	}

    @Test (dataProvider="getDataDenunciaFormal", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaDenunciaFormal(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil) throws Exception {

       /*  {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}
       * */
        ConsultaDenunciaFormalRequest consultaDenunciaFormalRequest = new ConsultaDenunciaFormalRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil);

        ConsultaDenunciaFormalResponse consultaDenunciaFormalResponse = atencionAlClienteService.consultaDenunciaFormal(consultaDenunciaFormalRequest);

        String derivar = consultaDenunciaFormalResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");

    }

    @Test (dataProvider="getDataCobranzas", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaCobranzas(String usuario, String verificacion, String pwid, String hash, String poliza, String cuit) throws Exception {

       /*  {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}
       * */
        ConsultaCobranzasRequest consultaCobranzasRequest = new ConsultaCobranzasRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuit);

        ConsultaCobranzasResponse consultaCobranzasResponse = atencionAlClienteService.consultaCobranzas(consultaCobranzasRequest);

        String derivar = consultaCobranzasResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataDiasBaja", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaDiasBaja(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro) throws Exception {
/*String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro
* {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}
* */
        ConsultaDiasBajaRequest consultaDiasBajaRequest = new ConsultaDiasBajaRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro);

        ConsultaDiasBajaResponse consultaDiasBajaResponse = atencionAlClienteService.consultaDiasBaja(consultaDiasBajaRequest);

        String derivar = consultaDiasBajaResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");

    }

    @Test (dataProvider="getDataFechaAudiencia", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaFechaAudiencia(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro) throws Exception {
	    /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}
	    * */

        ConsultaFechaAudienciaRequest consultaFechaAudienciaRequest  = new ConsultaFechaAudienciaRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro);

        ConsultaFechaAudienciaResponse consultaFechaAudienciaResponse = atencionAlClienteService.consultaFechaAudiencia(consultaFechaAudienciaRequest);

        String derivar = consultaFechaAudienciaResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test  (dataProvider="getDataFechaDictamen", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaFechaDictamen(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro) throws Exception {

  /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}
	    * */

        ConsultaFechaDictamenRequest consultaFechaDictamenRequest  = new ConsultaFechaDictamenRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro);

        ConsultaFechaDictamenResponse consultaFechaDictamenResponse = atencionAlClienteService.consultaFechaDictamen(consultaFechaDictamenRequest);

        String derivar = consultaFechaDictamenResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataFechaReingreso", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaFechaReingreso(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro) throws Exception {

 /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}
	    * */

        ConsultaFechaReingresoRequest consultaFechaReingresoRequest  = new ConsultaFechaReingresoRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro, fecha_siniestro);

        ConsultaFechaReingresoResponse consultaFechaReingresoResponse = atencionAlClienteService.consultaFechaReingreso(consultaFechaReingresoRequest);

        String derivar = consultaFechaReingresoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataLiquidacion", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaLiquidacion(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String periodo_desde, String periodo_hasta) throws Exception {

   /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String periodo_desde, String periodo_hasta
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17","21/07/17"}
	    * */

        ConsultaLiquidacionRequest consultaLiquidacionRequest  = new ConsultaLiquidacionRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro,periodo_desde, periodo_hasta);

        ConsultaLiquidacionResponse consultaLiquidacionResponse = atencionAlClienteService.consultaLiquidacion(consultaLiquidacionRequest);

        String derivar = consultaLiquidacionResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataMotivoRechazo", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaMotivoRechazo(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro) throws Exception {

   /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}
	    * */

        ConsultaMotivoRechazoRequest consultaMotivoRechazoRequest  = new ConsultaMotivoRechazoRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro, fecha_siniestro);

        ConsultaMotivoRechazoResponse consultaMotivoRechazoResponse = atencionAlClienteService.consultaMotivoRechazo(consultaMotivoRechazoRequest);

        String derivar = consultaMotivoRechazoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataNominaLog", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaNominaLog(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil) throws Exception {

   /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}
	    * */

        ConsultaNominaLogRequest consultaNominaLogRequest  = new ConsultaNominaLogRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil);

        ConsultaNominaLogResponse consultaNominaLogResponse = atencionAlClienteService.consultaNominaLog(consultaNominaLogRequest);

        String derivar = consultaNominaLogResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }



    @Test (dataProvider="getDataNomina", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaNomina(String usuario, String cuil) throws Exception {
 /* String usuario, String cuil
	    * {"art","T30663205931"}
	    * */

        ConsultaNominaRequest consultaNominaRequest  = new ConsultaNominaRequest( usuario,  cuil);

        ConsultaNominaResponse consultaNominaResponse = atencionAlClienteService.consultaNomina(consultaNominaRequest);

        String derivar = consultaNominaResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");

    }

    @Test (dataProvider="getDataNroContrato", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaNroContrato(String usuario, String verificacion, String pwid, String hash, String cuit) throws Exception {

   /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuit
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}
	    * */

        ConsultaNroContratoRequest consultaNroContratoRequest  = new ConsultaNroContratoRequest( usuario,  verificacion,  pwid,  hash,   cuit);

        ConsultaNroContratoResponse consultaNroContratoResponse = atencionAlClienteService.consultaNroContrato(consultaNroContratoRequest);

        String derivar = consultaNroContratoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataNroSiniestro", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaNroSiniestro(String cuil, String fecha_siniestro) throws Exception {

  /* String cuil, String fecha_siniestro
	    * {"T30663205931","21/02/17"}
	    * */

        ConsultaNroSiniestroRequest consultaNroSiniestroRequest  = new ConsultaNroSiniestroRequest(  cuil, fecha_siniestro);

        ConsultaNroSiniestroResponse consultaNroSiniestroResponse = atencionAlClienteService.consultaNroSiniestro(consultaNroSiniestroRequest);
        String derivar = consultaNroSiniestroResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataPrestadorMedico", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaPrestadorMedico(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro) throws Exception {

  /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro
            * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","21/02/17"}
	    * */

        ConsultaPrestadorMedicoRequest consultaPrestadorMedicoRequest  = new ConsultaPrestadorMedicoRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro, fecha_siniestro);

        ConsultaPrestadorMedicoResponse consultaPrestadorMedicoResponse = atencionAlClienteService.consultaPrestadorMedico(consultaPrestadorMedicoRequest);

        String derivar = consultaPrestadorMedicoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataProximoTurno", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaProximoTurno(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro) throws Exception {

 /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003"}
	    * */

        ConsultaProximoTurnoRequest consultaProximoTurnoRequest  = new ConsultaProximoTurnoRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro);

        ConsultaProximoTurnoResponse consultaProximoTurnoResponse = atencionAlClienteService.consultaProximoTurno(consultaProximoTurnoRequest);

        String derivar = consultaProximoTurnoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }

    @Test (dataProvider="getDataSaldo", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaSaldo(String usuario, String verificacion, String pwid, String hash, String poliza, String cuit) throws Exception {

        /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuit
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931"}
	    * */

        ConsultaSaldoRequest consultaSaldoRequest  = new ConsultaSaldoRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuit);

        ConsultaSaldoResponse consultaSaldoResponse = atencionAlClienteService.consultaSaldo(consultaSaldoRequest);

        String derivar = consultaSaldoResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }
    @Test (dataProvider="getDataSiniestro", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaSiniestro(String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro) throws Exception {

        /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil, String nro_siniestro, String fecha_siniestro
	    * {"art","nuevo","315", "uliywrzyxijrcrnswbxmfkgepqxjwuqy", "8801","T30663205931","09834209003","23/01/16"}
	    * */

        ConsultaSiniestroRequest consultaSiniestroRequest  = new ConsultaSiniestroRequest( usuario,  verificacion,  pwid,  hash,  poliza,  cuil, nro_siniestro,fecha_siniestro);

        ConsultaSiniestroResponse consultaSiniestroResponse = atencionAlClienteService.consultaSiniestro(consultaSiniestroRequest);

        String derivar = consultaSiniestroResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }
    @Test (dataProvider="getDataUsuarioExperta", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaUsuarioExperta(String dni, String usuario) throws Exception {

        /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro
	    * {"30663205931","art"}
	    * */

        ConsultaUsuarioExpertaRequest consultaUsuarioExpertaRequest  = new ConsultaUsuarioExpertaRequest( dni, usuario);

        ConsultaUsuarioExpertaResponse consultaUsuarioExpertaResponse = atencionAlClienteService.consultaUsuarioExperta(consultaUsuarioExpertaRequest);

        String derivar = consultaUsuarioExpertaResponse.getDerivar();

        assertEquals("El campo derivar es distinto del esperado ",derivar,"N");


    }
    @Test (dataProvider="getDataVerificacionAcc", dataProviderClass=ExpertaAtencionClienteTestData.class)
    public void testConsultaVerificacionAcc(String usuario, String verificacion, String fecha_siniestro, String cuil, String nro_siniestro, String cp, String tipo_accidente) throws Exception {

        /* String usuario, String verificacion, String pwid, String hash, String poliza, String cuil,  String nro_siniestro
	    * {"art","nuevo","23/01/16", "T30663205931", "8gfsg801","5000","uti"}
	    * */

        ConsultaVerificacionAccRequest consultaVerificacionAccRequest  = new ConsultaVerificacionAccRequest( usuario,  verificacion, fecha_siniestro,  cuil, nro_siniestro,cp,tipo_accidente);

        ConsultaVerificacionAccResponse consultaVerificacionAccResponse = atencionAlClienteService.consultaVerificacionAcc(consultaVerificacionAccRequest);

        String verificacionResponse = consultaVerificacionAccResponse.getVerificacion();

        assertEquals("El campo verificacion es distinto del esperado ",verificacionResponse,"nuevo");


    }

}
