package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaLiquidacionResponse extends AtencionAlClienteConsultaResponseAbs {

    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_ESTADO_LIQ" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="11" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    String estado_liq;

    public ConsultaLiquidacionResponse() {
    }

    public ConsultaLiquidacionResponse(String hash, String poliza, String derivar, String verificacion, String nroSiniestro,String estado_liq) {
        this.estado_liq = estado_liq;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.nroSiniestro = nroSiniestro;
    }
    public String getEstado_liq() {
        return estado_liq;
    }

    public void setEstado_liq(String estado_liq) {
        this.estado_liq = estado_liq;
    }
}
