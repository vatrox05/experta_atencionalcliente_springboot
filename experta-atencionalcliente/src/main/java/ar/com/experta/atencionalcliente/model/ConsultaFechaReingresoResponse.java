package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaFechaReingresoResponse extends AtencionAlClienteConsultaResponseAbs {
/* <complexType>
         <sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_REINGRESO" type="dateTime" db:index="9" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_NOMBRE_ACC" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="11" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/
    String fecha_reingreso;
    String nombre_acc;

    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaFechaReingresoResponse() {
    }

    public ConsultaFechaReingresoResponse(String hash, String poliza, String derivar, String verificacion, String nroSiniestro,String fecha_reingreso, String nombre_acc) {
        this.fecha_reingreso = fecha_reingreso;
        this.nombre_acc = nombre_acc;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.nroSiniestro = nroSiniestro;
    }

    public String getFecha_reingreso() {
        return fecha_reingreso;
    }

    public void setFecha_reingreso(Date fecha_reingreso)throws Exception {
        this.fecha_reingreso = alClienteUtils.dbToAppDateConverter(fecha_reingreso);
    }

    public String getNombre_acc() {
        return nombre_acc;
    }

    public void setNombre_acc(String nombre_acc) {
        this.nombre_acc = nombre_acc;
    }
}
