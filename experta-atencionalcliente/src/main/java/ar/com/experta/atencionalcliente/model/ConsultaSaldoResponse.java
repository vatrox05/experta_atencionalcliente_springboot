package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaSaldoResponse extends AtencionAlClienteConsultaResponseAbs {
    /* <complexType>
         <sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_CAPITAL" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_INTERES" type="string" db:index="8" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/

    String capital;
    String interes;

    public ConsultaSaldoResponse() {
    }

    public ConsultaSaldoResponse(String hash, String poliza, String derivar, String verificacion,String capital, String interes) {
        this.capital = capital;
        this.interes = interes;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getInteres() {
        return interes;
    }

    public void setInteres(String interes) {
        this.interes = interes;
    }
}
