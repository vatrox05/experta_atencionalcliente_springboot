package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaFechaAudienciaResponse extends AtencionAlClienteConsultaResponseAbs{

    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_AUDIENCIA" type="dateTime" db:index="8" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/

    String fecha_audiencia;
    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaFechaAudienciaResponse() {
    }

    public ConsultaFechaAudienciaResponse(String hash, String poliza, String derivar, String verificacion,String fecha_audiencia) {
        this.fecha_audiencia = fecha_audiencia;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public String getFecha_audiencia() {
        return fecha_audiencia;
    }

    public void setFecha_audiencia(Date fecha_audiencia) throws Exception{
        this.fecha_audiencia = alClienteUtils.dbToAppDateConverter(fecha_audiencia);

    }
}
