package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaSiniestroResponse extends AtencionAlClienteConsultaResponseAbs{
    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_ESTADO_STRO" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/


    String estado_stro;

    public ConsultaSiniestroResponse() {
    }

    public ConsultaSiniestroResponse(String hash, String poliza, String derivar, String verificacion, String nroSiniestro,String estado_stro) {
        this.estado_stro = estado_stro;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.nroSiniestro = nroSiniestro;
    }
    public String getEstado_stro() {
        return estado_stro;
    }

    public void setEstado_stro(String estado_stro) {
        this.estado_stro = estado_stro;
    }

}
