package ar.com.experta.atencionalcliente.configuration;

import ar.com.experta.atencionalcliente.monitoring.ControllerInterceptor;
import ar.com.experta.atencionalcliente.monitoring.ServiceInterceptor;
import ar.com.experta.atencionalcliente.service.AtencionAlClienteService;
import ar.com.experta.atencionalcliente.service.AtencionAlClienteServiceImpl;
import org.springframework.aop.framework.ProxyFactoryBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.*;
import org.springframework.web.filter.CommonsRequestLoggingFilter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

/**
 * Created by Vatrox10 on 7/5/2017.
 */

@EnableAutoConfiguration
@Configuration
@EnableAspectJAutoProxy
@ComponentScan("ar.com.experta.atencionalcliente")
public class AtencionAlClienteConfig extends WebMvcConfigurerAdapter {


    @Autowired
    ControllerInterceptor controllerInterceptor;


    @Bean
    public CommonsRequestLoggingFilter requestLoggingFilter() {
        CommonsRequestLoggingFilter loggingFilter = new CommonsRequestLoggingFilter();
        loggingFilter.setIncludeClientInfo(true);
        loggingFilter.setIncludeQueryString(true);
        loggingFilter.setIncludePayload(true);
        loggingFilter.setMaxPayloadLength(800);
        return loggingFilter;
    }
    @Bean
    public ServiceInterceptor serviceInterceptor() {
        return new ServiceInterceptor();
    }

}
