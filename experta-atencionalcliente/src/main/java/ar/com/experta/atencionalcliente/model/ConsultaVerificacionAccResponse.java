package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaVerificacionAccResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/
    String cp;


    public ConsultaVerificacionAccResponse() {
    }

    public ConsultaVerificacionAccResponse(String verificacion) {
        this.verificacion = verificacion;
    }

    public String getCp() {
        return cp;
    }

    public void setCp(String cp) {
        this.cp = cp;
    }
}
