package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;
import org.springframework.stereotype.Component;

import java.sql.Date;

@Component
public class ConsultaAlicuotasResponse extends AtencionAlClienteConsultaResponseAbs{


	String compFijo;
	String compVariable;
	String vigencia;

	AtencionAlClienteUtils atencionAlClienteUtils = AtencionAlClienteUtils.getInstance();

	public ConsultaAlicuotasResponse() {
	}

    public ConsultaAlicuotasResponse (String hash, String poliza, String derivar, String verificacion, String compFijo, String compVariable, String vigencia) {
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.compFijo = compFijo;
        this.compVariable = compVariable;
        this.vigencia = vigencia;
    }



    public String getCompFijo() {
		return compFijo;
	}
	public void setCompFijo(String compFijo) {
		this.compFijo = compFijo;
	}
	public String getCompVariable() {
		return compVariable;
	}
	public void setCompVariable(String compVariable) {
		this.compVariable = compVariable;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(Date vigencia) throws Exception {
		this.vigencia = atencionAlClienteUtils.dbToAppDateConverter(vigencia);
	}


}
