package ar.com.experta.atencionalcliente.service;

import ar.com.experta.atencionalcliente.model.*;
import org.springframework.stereotype.Repository;

@Repository	
public interface AtencionAlClienteService  {
	ConsultaAlicuotasResponse consultaAlicuotas(ConsultaAlicuotasRequest in) throws Exception;
	
	ConsultaDenunciaFormalResponse consultaDenunciaFormal(ConsultaDenunciaFormalRequest in) throws Exception;

	ConsultaDiasBajaResponse consultaDiasBaja(ConsultaDiasBajaRequest in) throws Exception;

	ConsultaCobranzasResponse consultaCobranzas(ConsultaCobranzasRequest in) throws Exception;

	ConsultaNominaResponse consultaNomina(ConsultaNominaRequest in) throws Exception;

	ConsultaNominaLogResponse consultaNominaLog(ConsultaNominaLogRequest in) throws Exception;

	ConsultaLiquidacionResponse consultaLiquidacion(ConsultaLiquidacionRequest in) throws Exception;

	ConsultaSiniestroResponse consultaSiniestro(ConsultaSiniestroRequest in) throws Exception;

	ConsultaFechaAudienciaResponse consultaFechaAudiencia(ConsultaFechaAudienciaRequest in) throws Exception;

	ConsultaFechaDictamenResponse consultaFechaDictamen(ConsultaFechaDictamenRequest in) throws Exception;

	ConsultaFechaReingresoResponse consultaFechaReingreso(ConsultaFechaReingresoRequest in) throws Exception;

	ConsultaMotivoRechazoResponse consultaMotivoRechazo(ConsultaMotivoRechazoRequest in) throws Exception;

	ConsultaNroContratoResponse consultaNroContrato(ConsultaNroContratoRequest in) throws Exception;

	ConsultaNroSiniestroResponse consultaNroSiniestro(ConsultaNroSiniestroRequest in) throws Exception;

	ConsultaPrestadorMedicoResponse consultaPrestadorMedico(ConsultaPrestadorMedicoRequest in) throws Exception;

	ConsultaProximoTurnoResponse consultaProximoTurno(ConsultaProximoTurnoRequest in) throws Exception;

	ConsultaSaldoResponse consultaSaldo(ConsultaSaldoRequest in) throws Exception;

	ConsultaUsuarioExpertaResponse consultaUsuarioExperta(ConsultaUsuarioExpertaRequest in) throws Exception;

	ConsultaVerificacionAccResponse consultaVerificacionAcc(ConsultaVerificacionAccRequest in) throws Exception;
}
