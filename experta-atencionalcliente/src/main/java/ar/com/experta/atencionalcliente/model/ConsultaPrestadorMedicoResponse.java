package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaPrestadorMedicoResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_NRO_SINIESTRO" type="string" db:index="7" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_PRESTADOR_MEDICO" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="10" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/
    String prestador_medico;

    public ConsultaPrestadorMedicoResponse() {
    }

    public ConsultaPrestadorMedicoResponse(String hash, String poliza, String derivar, String verificacion, String nroSiniestro,String prestador_medico) {
        this.prestador_medico = prestador_medico;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
        this.nroSiniestro = nroSiniestro;
    }

    public String getPrestador_medico() {
        return prestador_medico;
    }

    public void setPrestador_medico(String prestador_medico) {
        this.prestador_medico = prestador_medico;
    }
}
