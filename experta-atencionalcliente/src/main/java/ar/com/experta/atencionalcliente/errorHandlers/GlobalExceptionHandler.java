package ar.com.experta.atencionalcliente.errorHandlers;

import java.sql.SQLException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class GlobalExceptionHandler {

	@Autowired
	MessageSource messageSource;
	 
	private static final Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	@ExceptionHandler(Exception.class)
	ResponseEntity<?> handleSQLException(HttpServletRequest request, Exception ex){
		logger.info("SQLException Occured:: URL="+request.getRequestURL());
		return response(HttpStatus.BAD_REQUEST, "DB101","Error en la execucion de Base de Datos", ex.getMessage(),request.getRequestURL().toString());
	}

	
	@ResponseBody
	@ExceptionHandler(AtencionAlClienteException.class)
	ResponseEntity<?> handleAtencionAlClienteException(HttpServletRequest request,AtencionAlClienteException e){
        return response(HttpStatus.BAD_REQUEST, "CA101", e.getMessage());
    }
	
	
	
	private ResponseEntity<ServiceException> response(HttpStatus status, String code, String msg) {
        return response(status, code, msg, "", "");
    }

    private ResponseEntity<ServiceException> response(HttpStatus status, String code, String msg, String runtime, String location) {
        return new ResponseEntity<>(new ServiceException(code, msg, runtime, location), status);
    }
    
    
    
    
}