package ar.com.experta.atencionalcliente.model;

import ar.com.experta.atencionalcliente.utils.AtencionAlClienteUtils;

import java.sql.Date;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
public class ConsultaFechaDictamenResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<complexType>
         <sequence>
            <element name="AS_VERIFICACION" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_HASH" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_POLIZA" type="string" db:index="5" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AD_FECHA_DICTAMEN" type="dateTime" db:index="8" db:type="DATE" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="9" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/

    String fecha_dictamen;
    AtencionAlClienteUtils alClienteUtils = AtencionAlClienteUtils.getInstance();

    public ConsultaFechaDictamenResponse() {
    }

    public ConsultaFechaDictamenResponse(String hash, String poliza, String derivar, String verificacion,String fecha_dictamen) {
        this.fecha_dictamen = fecha_dictamen;
        this.hash = hash;
        this.poliza = poliza;
        this.derivar = derivar;
        this.verificacion = verificacion;
    }

    public String getFecha_dictamen() {
        return fecha_dictamen;
    }

    public void setFecha_dictamen(Date fecha_dictamen) throws Exception{
        this.fecha_dictamen = alClienteUtils.dbToAppDateConverter(fecha_dictamen);
    }
}
