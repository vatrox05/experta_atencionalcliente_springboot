package ar.com.experta.atencionalcliente.model;

import org.springframework.stereotype.Component;

@Component
public class ConsultaAlicuotasRequest extends AtencionAlClienteConsultaRequestAbs{


	public ConsultaAlicuotasRequest(String usuario, String verificacion, String pwid, String hash, String poliza, String cuit) {
		this.usuario = usuario;
		this.verificacion = verificacion;
		this.pwid = pwid;
		this.hash = hash;
		this.poliza = poliza;
		this.cuit = cuit;
	}

	public ConsultaAlicuotasRequest() {
	}




}
