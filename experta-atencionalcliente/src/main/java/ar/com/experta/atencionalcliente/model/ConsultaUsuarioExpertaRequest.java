package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaUsuarioExpertaRequest extends AtencionAlClienteConsultaRequestAbs{
    /* <complexType>
         <sequence>
            <element name="AS_USUARIO" type="string" db:index="1" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DNI" type="string" db:index="2" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>
      </complexType>*/
    String dni;

    public ConsultaUsuarioExpertaRequest() {
    }

    public ConsultaUsuarioExpertaRequest(String dni, String usuario) {
        this.dni = dni;
        this.usuario = usuario;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }
}
