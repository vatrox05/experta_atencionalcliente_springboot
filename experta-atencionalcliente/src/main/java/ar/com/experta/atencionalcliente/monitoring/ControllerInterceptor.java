package ar.com.experta.atencionalcliente.monitoring;

import ar.com.experta.atencionalcliente.controller.AtencionAlClienteController;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Vatrox10 on 6/30/2017.
 */
@Component
public class ControllerInterceptor implements HandlerInterceptor {
    private final static Logger LOGGER = Logger.getLogger(AtencionAlClienteController.class.getCanonicalName());

    public ControllerInterceptor() {
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        LOGGER.log(Level.FINE, "HttpServletRequest "+request.getRequestURI()+" - ");
        //if this is false, there is no response
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler, ModelAndView modelAndView) throws Exception {
//        LOGGER.log(Level.FINE, "HttpServletRequest "+request.getRequestURI()+" - ");
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
//        LOGGER.log(Level.INFO, "HttpServletRequest "+response.getStatus()+" - handler: "+handler.toString());
    }
}
