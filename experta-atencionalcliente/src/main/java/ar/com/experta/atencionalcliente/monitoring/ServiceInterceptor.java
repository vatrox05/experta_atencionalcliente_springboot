package ar.com.experta.atencionalcliente.monitoring;

import ar.com.experta.atencionalcliente.service.AtencionAlClienteService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;

@Aspect
@Component
public class ServiceInterceptor {
    private org.apache.log4j.Logger log = org.apache.log4j.Logger.getLogger(AtencionAlClienteService.class.getName());

    @Around("execution(* *(..)) && @annotation(com.jcabi.aspects.Loggable)")
    public Object around(ProceedingJoinPoint point) throws Exception {
        long start = System.currentTimeMillis();

        Object pointObj = point.getArgs()[0];

        printLog(point, pointObj,start);

        Object result = null;

        try {
            result = point.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return result;
    }

    @AfterReturning(pointcut ="execution(* *(..)) && @annotation(com.jcabi.aspects.Loggable)",
                    returning= "result")
    public void afterRunning(JoinPoint joinPoint, Object result) throws Exception {
        long start = System.currentTimeMillis();

        String resultJson = getObjectToJson(result);

        log.debug("Response from Method "+
                MethodSignature.class.cast(joinPoint.getSignature()).getMethod().getName()
                +"\tArguments: "+
                resultJson
                +"\tTime: "+
                String.valueOf(System.currentTimeMillis() - start)
        );

    }

    private void printLog(ProceedingJoinPoint point, Object pointObj,long startTime) throws JsonProcessingException {

        String jsonInString = getObjectToJson(pointObj);

       log.debug("Resquest from Method: "+
               MethodSignature.class.cast(point.getSignature()).getMethod().getName()
                +"\tArguments: "+
                jsonInString
               +"\tTime: "+
                String.valueOf(System.currentTimeMillis() - startTime)
        );

    }

    private String getObjectToJson(Object pointObj) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();

        // Convert object to JSON string and pretty print
        String jsonInString = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(pointObj);

        return jsonInString;
    }
}