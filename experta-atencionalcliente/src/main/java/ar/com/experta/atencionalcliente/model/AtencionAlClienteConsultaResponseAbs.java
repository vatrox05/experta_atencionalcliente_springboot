package ar.com.experta.atencionalcliente.model;

import org.springframework.stereotype.Component;

/**
 * Created by Vatrox10 on 26/06/2017.
 */
@Component
abstract public class AtencionAlClienteConsultaResponseAbs  {

    String hash;
    String poliza;
    String derivar;
    String verificacion;
    String nroSiniestro;

    public String getHash() {
        return hash;
    }
    public void setHash(String hash) {
        this.hash = hash;
    }
    public String getPoliza() {
        return poliza;
    }
    public void setPoliza(String poliza) {
        this.poliza = poliza;
    }
    public String getDerivar() {
        return derivar;
    }
    public void setDerivar(String derivar) {
        this.derivar = derivar;
    }
    public String getVerificacion() {
        return verificacion;
    }
    public void setVerificacion(String verificacion) {
        this.verificacion = verificacion;
    }
    public String getNroSiniestro() {
        return nroSiniestro;
    }
    public void setNroSiniestro(String nroSiniestro) {
        this.nroSiniestro = nroSiniestro;
    }

}
