package ar.com.experta.atencionalcliente.controller;


import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

import ar.com.experta.atencionalcliente.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestBody;

import ar.com.experta.atencionalcliente.errorHandlers.AtencionAlClienteException;
import ar.com.experta.atencionalcliente.service.AtencionAlClienteService;

@RestController
@RequestMapping("/servicios/atencionalcliente")
public class AtencionAlClienteController {
    private final static Logger LOGGER = Logger.getLogger(AtencionAlClienteController.class.getName());
    
    @Autowired
    AtencionAlClienteService atencionAlClienteService;

    
    @RequestMapping(method = RequestMethod.POST, value = "/alicuotas")
    public ResponseEntity<?> consultaAlicuotas(@RequestBody ConsultaAlicuotasRequest car) throws Exception {
        if (StringUtils.isEmpty(car.getCuit())){
        	throw new AtencionAlClienteException("Ingrese el cuit del usuario");
        }
    	
    	LOGGER.log(Level.FINE, "Ingresando Consulta Alicuotas: " + car.getCuit());
        try{
        	
	        ConsultaAlicuotasResponse result = atencionAlClienteService.consultaAlicuotas(car);
	        LOGGER.log(Level.FINE, "Ejecutando Consulta Alicuotas");
	        return new ResponseEntity<ConsultaAlicuotasResponse>(result, HttpStatus.OK);
	        
        }catch (Exception e ){
            LOGGER.log(Level.FINE, "Error ejecutando Consulta Alicuotas"+e.getMessage());
        	throw new Exception(e.getMessage());
        }

        
    }
    @RequestMapping(method = RequestMethod.POST, value = "/denunciaformal")
    public ResponseEntity<?> consultaDenunciaFormal(@RequestBody ConsultaDenunciaFormalRequest car) throws Exception {
    	
    	if (StringUtils.isEmpty(car.getCuil())){
        	throw new AtencionAlClienteException("Ingrese el cuil del usuario");
        }
    	
    	LOGGER.log(Level.FINE, "Ingresando Consulta Denuncia Formal: " + car.getCuil());
        
       try{
	        ConsultaDenunciaFormalResponse result = atencionAlClienteService.consultaDenunciaFormal(car);
	        LOGGER.log(Level.FINE, "Ejecutando Consulta Denuncia Formal");

	        return new ResponseEntity<ConsultaDenunciaFormalResponse>(result, HttpStatus.OK);
	        
	   }catch (Exception e ){
        	
        	throw new Exception(e.getMessage());
        	
       }
       
    }
    @RequestMapping(method = RequestMethod.POST, value = "/estudiocobranzas")
    public ResponseEntity<?> consultaCobranzas(@RequestBody ConsultaCobranzasRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuit())){
            throw new AtencionAlClienteException("Ingrese el cuit del usuario");
        }

        try{
            ConsultaCobranzasResponse result = atencionAlClienteService.consultaCobranzas(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaCobranzas ");

            return new ResponseEntity<ConsultaCobranzasResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/diasbaja")
    public ResponseEntity<?> consultaDiasBaja(@RequestBody ConsultaDiasBajaRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la Poliza del usuario");
        }

        try{
            ConsultaDiasBajaResponse result = atencionAlClienteService.consultaDiasBaja(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaDiasBaja");

            return new ResponseEntity<ConsultaDiasBajaResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/turnocm")
    public ResponseEntity<?> consultaFechaAudiencia(@RequestBody ConsultaFechaAudienciaRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la Poliza del usuario");
        }

        try{
            ConsultaFechaAudienciaResponse result = atencionAlClienteService.consultaFechaAudiencia(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaFechaAudiencia");

            return new ResponseEntity<ConsultaFechaAudienciaResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/recepciondictamen")
    public ResponseEntity<?> consultaFechaDictamen(@RequestBody ConsultaFechaDictamenRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la Poliza del usuario");
        }

        try{
            ConsultaFechaDictamenResponse result = atencionAlClienteService.consultaFechaDictamen(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaFechaDictamen ");

            return new ResponseEntity<ConsultaFechaDictamenResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/reingreso")
    public ResponseEntity<?> consultaFechaReingreso(@RequestBody ConsultaFechaReingresoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuil())){
            throw new AtencionAlClienteException("Ingrese el cuil del usuario");
        }

        try{
            ConsultaFechaReingresoResponse result = atencionAlClienteService.consultaFechaReingreso(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaFechaReingreso ");

            return new ResponseEntity<ConsultaFechaReingresoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/estadoliquidacion")
    public ResponseEntity<?> consultaLiquidacion(@RequestBody ConsultaLiquidacionRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaLiquidacionResponse result = atencionAlClienteService.consultaLiquidacion(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaLiquidacion");

            return new ResponseEntity<ConsultaLiquidacionResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/motivorechazo")
    public ResponseEntity<?> consultaMotivoRechazo(@RequestBody ConsultaMotivoRechazoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaMotivoRechazoResponse result= atencionAlClienteService.consultaMotivoRechazo(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaMotivoRechazo");

            return new ResponseEntity<ConsultaMotivoRechazoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/enexperta")
    public ResponseEntity<?> consultaNomina(@RequestBody ConsultaNominaRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuil())){
            throw new AtencionAlClienteException("Ingrese el cuil del usuario");
        }

        try{
            ConsultaNominaResponse result = atencionAlClienteService.consultaNomina(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaNomina");

            return new ResponseEntity<ConsultaNominaResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/ennomina")
    public ResponseEntity<?> consultaNominaLog(@RequestBody ConsultaNominaLogRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaNominaLogResponse result = atencionAlClienteService.consultaNominaLog(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaNominaLog");

            return new ResponseEntity<ConsultaNominaLogResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/nrocontrato")
    public ResponseEntity<?> consultaNroContrato(@RequestBody ConsultaNroContratoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuit())){
            throw new AtencionAlClienteException("Ingrese el cuit del usuario");
        }

        try{
            ConsultaNroContratoResponse result = atencionAlClienteService.consultaNroContrato(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaNroContrato");

            return new ResponseEntity<ConsultaNroContratoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/nrosiniestro")
    public ResponseEntity<?> consultaNroSiniestro(@RequestBody ConsultaNroSiniestroRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuil())){
            throw new AtencionAlClienteException("Ingrese el cuil del usuario");
        }

        try{
            ConsultaNroSiniestroResponse result = atencionAlClienteService.consultaNroSiniestro(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaNroSiniestro");

            return new ResponseEntity<ConsultaNroSiniestroResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/prestadormedico")
    public ResponseEntity<?> consultaPrestadorMedico(@RequestBody ConsultaPrestadorMedicoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaPrestadorMedicoResponse result = atencionAlClienteService.consultaPrestadorMedico(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaPrestadorMedico");

            return new ResponseEntity<ConsultaPrestadorMedicoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/proximoturno")
    public ResponseEntity<?> consultaProximoTurno(@RequestBody ConsultaProximoTurnoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaProximoTurnoResponse result = atencionAlClienteService.consultaProximoTurno(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaProximoTurno");

            return new ResponseEntity<ConsultaProximoTurnoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/saldo")
    public ResponseEntity<?> consultaSaldo(@RequestBody ConsultaSaldoRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaSaldoResponse result = atencionAlClienteService.consultaSaldo(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaSaldo");

            return new ResponseEntity<ConsultaSaldoResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/estadosiniestro")
    public ResponseEntity<?> consultaSiniestro(@RequestBody ConsultaSiniestroRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getPoliza())){
            throw new AtencionAlClienteException("Ingrese la poliza del usuario");
        }

        try{
            ConsultaSiniestroResponse result = atencionAlClienteService.consultaSiniestro(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaSiniestro");

            return new ResponseEntity<ConsultaSiniestroResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/usuarioexperta")
    public ResponseEntity<?> consultaUsuarioExperta(@RequestBody ConsultaUsuarioExpertaRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuit())){
            throw new AtencionAlClienteException("Ingrese el cuit del usuario");
        }

        try{
            ConsultaUsuarioExpertaResponse result = atencionAlClienteService.consultaUsuarioExperta(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaUsuarioExperta");

            return new ResponseEntity<ConsultaUsuarioExpertaResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }


    }
    @RequestMapping(method = RequestMethod.POST, value = "/verificacionacc")
    public ResponseEntity<?> consultaVerificacionAcc(@RequestBody ConsultaVerificacionAccRequest car) throws Exception {

        if (StringUtils.isEmpty(car.getCuil())){
            throw new AtencionAlClienteException("Ingrese el cuil del usuario");
        }

        try{
            ConsultaVerificacionAccResponse result = atencionAlClienteService.consultaVerificacionAcc(car);
            LOGGER.log(Level.FINE, "Ejecutando consultaVerificacionAcc");

            return new ResponseEntity<ConsultaVerificacionAccResponse>(result, HttpStatus.OK);

        }catch (Exception e ){

            throw new Exception(e.getMessage());

        }
    }
}
