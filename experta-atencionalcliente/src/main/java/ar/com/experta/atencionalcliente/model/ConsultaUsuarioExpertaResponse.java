package ar.com.experta.atencionalcliente.model;

/**
 * Created by Vatrox10 on 27/06/2017.
 */
public class ConsultaUsuarioExpertaResponse extends AtencionAlClienteConsultaResponseAbs {
    /*<sequence>
            <element name="AS_EXISTE_USUARIO" type="string" db:index="3" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
            <element name="AS_DERIVAR" type="string" db:index="4" db:type="VARCHAR2" minOccurs="0" nillable="true"/>
         </sequence>*/
    String existe_usuario;

    public ConsultaUsuarioExpertaResponse() {
    }

    public ConsultaUsuarioExpertaResponse(String existe_usuario, String deivar) {
        this.existe_usuario = existe_usuario;
        this.derivar = deivar;
    }

    public String getExiste_usuario() {
        return existe_usuario;
    }

    public void setExiste_usuario(String existe_usuario) {
        this.existe_usuario = existe_usuario;
    }
}
